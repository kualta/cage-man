﻿using System;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    [SerializeField]
    internal NPCAnimationController animation;

    public delegate void ActiveState();
    public ActiveState activeState;

    public float hitPoints;

    Rigidbody rigidBody;
    float currentVelocity;


    public void SetState(Action state) {
        activeState = new ActiveState(state);
    }

    public void onGettingHit() {
        print("OnGettingHit (Controller)");
        int damageTaken = UnityEngine.Random.Range(11, 28);
        print("Previous HP = " + hitPoints);
        ReduceHitPoints(damageTaken);
        print("HP = " + hitPoints);
        SetState(GetHit);
        int randomizeAnimation = UnityEngine.Random.Range(1, 3);
        animation.onGettingHit(randomizeAnimation, hitPoints);
    }

    internal void ReduceHitPoints(int reduceAmount) {
        hitPoints -= reduceAmount;
    }

    void Start() {
        if (GetComponent<Rigidbody>())
            rigidBody = GetComponent<Rigidbody>();
        else
            Debug.LogError("No RigidBody found");

        hitPoints = 100f;

        SetState(Block);
    }

    void Update() {
        UpdateState();
        activeState();
    }

    void UpdateState() {
    }

    void GetHit() {
        currentVelocity = 0;
        rigidBody.velocity = Vector3.zero;
    }

    void Block() {
        currentVelocity = 0;
        rigidBody.velocity = Vector3.zero;
    }
}
