﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCollisionController : MonoBehaviour
{
    CapsuleCollider characterCollider;
    NPCAnimationController animation;

    [SerializeField]
    internal NPCController controller;


    void Start() {
        if (GetComponent<CapsuleCollider>())
            characterCollider = GetComponent<CapsuleCollider>();
        else
            Debug.LogError("No Capsule Collider found");

        animation = controller.GetComponent<NPCAnimationController>();
    }

    void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.tag == "Weapon") {
            controller.onGettingHit();
            print("OnTriggerEnter");
        }
    }
}
