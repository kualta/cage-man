﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAnimationController : MonoBehaviour
{
    [SerializeField]
    internal NPCController controller;

    Animator anim;

    // Animator Hashes
    internal int gettingHitHash = Animator.StringToHash("Hit");
    internal int hitPointsHash = Animator.StringToHash("HP");

    public void onGettingHit(int gettingHitAnimationNumber, float hitPoints) {
        print("onGettingHit (AnimationController)");
        anim.SetInteger(gettingHitHash, gettingHitAnimationNumber);
        anim.SetFloat(hitPointsHash, hitPoints);
    }

    void Start() {
        if (GetComponent<Animator>())
            anim = GetComponent<Animator>();
        else
            Debug.LogError("No Animator found");
    }

    void OnGotHitAnimEnd() {
        anim.SetInteger(gettingHitHash, 0);
    }
}
