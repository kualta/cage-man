﻿using System;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    [SerializeField]
    internal PlayerController controller;

    PlayerInputController input;
    PlayerAnimationController animation;

    public float walkVelocity = 1.9f;
    public float runVelocity = 2.5f;
    public float rotationVelocity = 100;

    public float runningInput = 0.8f;
    public float walkingInput = 0.3f;
    public float deadZoneInput = 0.1f;

    [Header("Jump Settings")]
    public float jumpVelocity = 5f;
    public float fallMultiplier = 2.5f;
    public ForceMode forceMode = ForceMode.Impulse;

    public bool isFalling;
    public bool isAttacking;
    public bool isRunning;
    public bool turnLock;

    public delegate void ActiveState();
    public ActiveState activeState;

    Quaternion targetRotation;
    Vector3 rotationTarget;
    float currentVelocity;
    Rigidbody rigidBody;


    public void SetState(Action state) {
        activeState = new ActiveState(state);
    }

    public void OnAttackRegular() {
        turnLock = true;
        isAttacking = true;
        animation.anim.SetBool(animation.regularAttackHash, true);
        controller.collision.OnAttack();
        SetState(AttackRegular);
    }

    void Start()
    {
        SetState(Idle);
        animation = controller.animation;
        input = controller.input;
        if (GetComponent<Rigidbody>())
            rigidBody = GetComponent<Rigidbody>();
        else
            Debug.LogError("No RigidBody found");

        targetRotation = transform.rotation;
        isFalling = false;
        isAttacking = false;
        turnLock = false;
    }

    void Update() {
        if (!turnLock) {
            Turn();
        }
    }

    /*
     NOTE: UpdateState() is supposed to be in FixedUpdate() because it has to be
     synced with ground checking, otherwise jumping animation fails.
    */

    void FixedUpdate() {
        activeState();
        UpdateState();
    }

    void UpdateState() {
        animation.anim.SetFloat(animation.speedHash, currentVelocity);
        if (!isAttacking) {
            if (input.fullInput > deadZoneInput && input.fullInput < runningInput) {
                SetState(Walk);
            } else if (input.fullInput >= runningInput) {
                SetState(Run);
            } else if (!(rigidBody.velocity.y < 0) && !isFalling){
                SetState(Idle);
            } else if (input.fullInput == 0){
                SetState(Idle);
            }
        }

        if (!isAttacking && input.regularAttackInput) {
            OnAttackRegular();
        }

        if (Input.GetButtonDown("Jump") && !isFalling) {
            Jump(jumpVelocity, forceMode);
        }

        if (rigidBody.velocity.y < 0) {
            rigidBody.AddForce(Vector3.up * Physics.gravity.y * (fallMultiplier - 1));
        }
    }

    void AttackRegular() {
        currentVelocity = 0;
        rigidBody.velocity = Vector3.zero;
    }

    void Turn() {
        rotationTarget = input.cameraForward * input.forwardInput + input.cameraRight * input.turnInput;
        targetRotation = Quaternion.LookRotation(rotationTarget);
        if (rotationTarget != Vector3.zero)
            rigidBody.MoveRotation(Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationVelocity));
    }

    void Walk() {
        currentVelocity = walkVelocity;
        rigidBody.MovePosition(transform.position + transform.forward * walkVelocity * Time.deltaTime);
    }

    void Jump(float jumpForce, ForceMode forceMode) {
        // Debug.Log(UnityEngine.StackTraceUtility.ExtractStackTrace());
        animation.anim.SetBool(animation.jumpHash, true);
        Debug.Log(animation.anim.GetBool(animation.jumpHash));
        rigidBody.AddForce(Vector3.up * jumpForce, forceMode);
        isFalling = true;
    }

    void Idle() {
        currentVelocity = 0;
        rigidBody.velocity = Vector3.zero;
    }

    void Run() {
        currentVelocity = runVelocity;
        rigidBody.MovePosition(transform.position + transform.forward * runVelocity * Time.deltaTime);
    }


}
