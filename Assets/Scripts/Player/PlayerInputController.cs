﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    [SerializeField]
    internal PlayerController controller;

    Vector3 fullInputVector;
    public Vector3 cameraForward;
    public Vector3 cameraRight;
    public float turnInput;
    public float fullInput;
    public float forwardInput;
    public bool regularAttackInput;

    void Start()
    {
        turnInput = 0;
        fullInput = 0;
        forwardInput = 0;
    }

    void Update()
    {
        GetInput();
        // JoystickDebug();
    }

    void JoystickDebug() {
        for (int i = 0;i < 20; i++) {
            if(Input.GetKeyDown("joystick 1 button "+i)){
                print("joystick 1 button "+i);
            }
        }
        if (Input.GetAxis("RegularAttack") > 0f) {
            print(Input.GetAxis("RegularAttack"));
        }
        print(regularAttackInput);
    }
    void GetInput() {
        regularAttackInput = Input.GetButton("RegularAttack");
        // Debug.Log("RegularAttack: " + Input.GetButton("RegularAttack"));
        // Debug.Log("RegularAttackJoystick: " + Input.GetAxis("RegularAttackJoystick"));
        // Debug.Log(regularAttackInput);
        if (Input.GetAxis("RegularAttackJoystick") > 0.4f) {
            // Debug.Log("RegularAttackJoystick: " + Input.GetAxis("RegularAttackJoystick"));
            regularAttackInput = true;
        }
        forwardInput = Input.GetAxis("Vertical");
        turnInput = Input.GetAxis("Horizontal");
        fullInput = Mathf.Clamp01(new Vector2(forwardInput, turnInput).magnitude);
        cameraForward = controller.camera.transform.forward;
        cameraRight = controller.camera.transform.right;
        cameraForward.y = 0f;
        cameraRight.y = 0f;
        cameraForward.Normalize();
        cameraRight.Normalize();
        fullInputVector = new Vector3(turnInput, 0, forwardInput);
    }
}
