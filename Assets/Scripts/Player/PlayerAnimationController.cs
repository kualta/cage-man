﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{

    [SerializeField]
    internal PlayerController controller;

    public float DistanceToGround = 0.13f;
    public float rightFootRotation = 0.2f;
    public float leftFootRotation = 0.4f;

    public Animator anim;
    public LayerMask layerMask;

    // Animator Hashes
    public int speedHash = Animator.StringToHash("Speed");
    public int jumpHash = Animator.StringToHash("Jumping");
    public int footHash = Animator.StringToHash("Right Foot");
    public int regularAttackHash = Animator.StringToHash("Regular Attack");


    void Start()
    {
        if (GetComponent<Animator>())
            anim = GetComponent<Animator>();
        else
            Debug.LogError("No Animator found");
    }

    public void RegularAttackEnd() {
        anim.SetBool(regularAttackHash, false);
        anim.SetBool(footHash, false);
        controller.movement.isAttacking = false;
        controller.movement.turnLock = false;
        controller.collision.OnAttackEnd();
    }

    public void WalkRightAnimEnd() {
        anim.SetBool(footHash, false);
    }

    public void WalkLeftAnimEnd() {
        anim.SetBool(footHash, true);
    }

    public void WalkRightAnimStart() {
        anim.SetBool(footHash, true);
    }

    public void WalkLeftAnimStart() {
        anim.SetBool(footHash, false);
    }

    public void RunningRightAnimEnd() {
        anim.SetBool(footHash, false);
    }

    public void RunningLeftAnimEnd() {
        anim.SetBool(footHash, true);
    }


    private void OnAnimatorIK(int layerIndex) {
        anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, anim.GetFloat("IKLeftFootWeight"));
        anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, anim.GetFloat("IKLeftFootWeight"));
        anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, anim.GetFloat("IKRightFootWeight"));
        anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, anim.GetFloat("IKRightFootWeight"));

        anim.SetIKHintPositionWeight(AvatarIKHint.LeftKnee, anim.GetFloat("IKLeftFootWeight"));
        anim.SetIKHintPositionWeight(AvatarIKHint.RightKnee, anim.GetFloat("IKRightFootWeight"));

        RaycastHit hit;
        Ray ray = new Ray(anim.GetIKPosition(AvatarIKGoal.LeftFoot) + Vector3.up, 1.3f *Vector3.down);
        Debug.DrawRay(anim.GetIKPosition(AvatarIKGoal.LeftFoot) + Vector3.up,(DistanceToGround + 1.2f) * Vector3.down );
        if (Physics.Raycast(ray, out hit, DistanceToGround + 1.2f, layerMask)) {
            if (hit.transform.tag == "Floor") {
                Vector3 footPosition = hit.point;
                footPosition.y += DistanceToGround;

                anim.SetIKPosition(AvatarIKGoal.LeftFoot, footPosition);
                anim.SetIKRotation(AvatarIKGoal.LeftFoot, Quaternion.LookRotation(transform.forward + (rightFootRotation * -transform.right), hit.normal));
            }
        }

        ray = new Ray(anim.GetIKPosition(AvatarIKGoal.RightFoot) + Vector3.up, 1.3f * Vector3.down);
        Debug.DrawRay(anim.GetIKPosition(AvatarIKGoal.RightFoot) + Vector3.up,(DistanceToGround + 1.2f) * Vector3.down );
        if (Physics.Raycast(ray, out hit, DistanceToGround + 1.2f, layerMask)) {
            if (hit.transform.tag == "Floor") {
                Vector3 footPosition = hit.point;
                footPosition.y += DistanceToGround;

                anim.SetIKPosition(AvatarIKGoal.RightFoot, footPosition);
                anim.SetIKRotation(AvatarIKGoal.RightFoot, Quaternion.LookRotation(transform.forward + (leftFootRotation * transform.right), hit.normal));
            }
        }
    }
}
