﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionsController : MonoBehaviour
{
    [SerializeField]
    internal PlayerController controller;

    public GameObject swordColliderObj;
    public float maxIncline = 45.0f;
    public bool isGrounded;

    CapsuleCollider swordCollider;
    CapsuleCollider characterCollider;
    PlayerMovementController movement;
    PlayerAnimationController animation;
    float distanceToGround;

    public void OnAttack() {
        swordCollider.enabled = true;
    }

    public void OnAttackEnd() {
        swordCollider.enabled = false;
    }

    void Start()
    {
        if (GetComponent<CapsuleCollider>())
            characterCollider = GetComponent<CapsuleCollider>();
        else
            Debug.LogError("No Capsule Collider found");

        movement = controller.GetComponent<PlayerMovementController>();
        animation = controller.GetComponent<PlayerAnimationController>();
        swordCollider = swordColliderObj.GetComponent<CapsuleCollider>();
    }

    void FixedUpdate() {
     Debug.DrawRay(transform.position + new Vector3(0, 1, 0), Vector3.down * (distanceToGround + 0.1f));
     CheckForGround();
     HandleGround();
    }

    void CheckForGround() {
        distanceToGround = characterCollider.bounds.extents.y;
        isGrounded = Physics.Raycast(transform.position + new Vector3(0, 1, 0), Vector3.down, distanceToGround + 0.1f);
    }

    void HandleGround() {
        Debug.Log(UnityEngine.StackTraceUtility.ExtractStackTrace());
        if (isGrounded) {
            movement.isFalling = false;
            animation.anim.SetBool(animation.jumpHash, false);
        }
    }

}
