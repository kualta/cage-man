﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform camera;

    [SerializeField]
    internal PlayerInputController input;

    [SerializeField]
    internal PlayerCollisionsController collision;

    [SerializeField]
    internal PlayerAnimationController animation;

    [SerializeField]
    internal PlayerMovementController movement;

    [SerializeField]
    internal GUIController gui;
   
}
