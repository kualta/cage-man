﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxCameraController : MonoBehaviour
{
    [SerializeField]
    private Transform mainCamera;

    void Start()
    {
        
    }

    void FixedUpdate()
    {
        transform.rotation = mainCamera.rotation;
    }
}
