﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIController : MonoBehaviour
{
    [SerializeField]
    internal PlayerController controller;

    void OnGUI() {
        if (controller.collision.isGrounded) {
            GUI.Label(new Rect(10, 10, 100, 20), "Grounded");
        } else {
            GUI.Label(new Rect(10, 10, 100, 20), "Not Grounded");
        }
    }
}
