﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public float lookSmooth = 0.09f;
    public Vector3 offsetFromTarget = new Vector3(-0.4f, 2.6f, -3);
    public float rotateVelocity = 15f;
    public float upperVerticalClamp = 15f;
    public float bottomVerticalClamp = 5f;

    float targetAngle;
    float rotationH;
    float rotationV;
    Quaternion rotation;
    Vector3 position = Vector3.zero;

    void Start()
    {
        SetCameraTarget(target);
    }

    public void SetCameraTarget(Transform newTarget) {
        target = newTarget;
    }

    void Update() {
        GetInput();
    }

    void LateUpdate() {
        RotateCamera();
        MoveCamera();
    }

    void GetInput() {
        float lastRotation = rotationV;
        rotationH += Input.GetAxis("RightH") * rotateVelocity;
        rotationV += Input.GetAxis("RightV") * rotateVelocity;
        if (rotationV != lastRotation) {
            rotationV = Mathf.Clamp(rotationV, bottomVerticalClamp, upperVerticalClamp);
        }
        rotation = Quaternion.Euler(rotationV, rotationH, 0f);
    }

    void MoveCamera() {
        position = rotation * offsetFromTarget + target.position;
        transform.position = position;
    }

    void RotateCamera() {
        transform.rotation = rotation;
    }
}
